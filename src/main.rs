extern crate sdl2;

use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::video::Window;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use std::time::Duration;



fn main() {



    let square_size: &str = "10";
    let square_size_u32: u32 = square_size.parse().unwrap();
    let square_size_i32: i32 = square_size.parse().unwrap();

    let window_height: &str = "200";
    let window_width: &str = "200";
    let window_height_i32: i32 = window_height.parse().unwrap();
    let window_width_i32: i32 = window_width.parse().unwrap();

    let window_height_u32: u32 = window_height.parse().unwrap();
    let window_width_u32: u32 = window_width.parse().unwrap();

    let mut limiter: bool = true;
    let mut current_direction: &str = "none";



    let center: [i32; 2] = [
        (window_height_i32 / 2),
        (window_width_i32 / 2),
    ];

    let mut square_location = [center[0], center[1]];
    let mut snake_length = 1;
    let mut snake_tale_location = [square_location[0], square_location[1]];




    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();





    let window = video_subsystem.window("Snake", window_height_u32, window_width_u32).build().unwrap();

    let mut canvas : Canvas<Window> = window.into_canvas().build().unwrap();

    canvas.set_draw_color(Color::RGB(255, 255, 255));
    canvas.present();

    let mut event_pump = sdl_context.event_pump().unwrap();



    'running: loop {

        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown {keycode: Some(Keycode::Escape), .. } => { break 'running },

                Event::KeyDown {keycode: Some(Keycode::Left), ..} => {
                    if limiter == true {
                        current_direction = "left";
                    }
                },

                Event::KeyDown {keycode: Some(Keycode::Right), ..} => {
                    if limiter == true {
                        current_direction = "right";
                    }
                },

                Event::KeyDown {keycode: Some(Keycode::Up), ..} => {
                    if limiter == true {
                        current_direction = "up";
                    }
                },

                Event::KeyDown {keycode: Some(Keycode::Down), ..} => {
                    if limiter == true {
                        current_direction = "down";
                    }
                },
                _ => {}
            }
        }

        if current_direction == "left" && limiter == true {
            square_location[0] = square_location[0] - square_size_i32;
        } else if current_direction == "right" && limiter == true {
            square_location[0] = square_location[0] + square_size_i32;
        } else if current_direction == "up" && limiter == true {
            square_location[1] = square_location[1] - square_size_i32;
        } else if current_direction == "down" && limiter == true {
            square_location[1] = square_location[1] + square_size_i32;
        }
        limiter = false;

        if square_location[1] < 0 || square_location[1] > window_height_i32 - square_size_i32 || square_location[0] < 0 || square_location[0] > window_width_i32  - square_size_i32 {
            square_location = center;
            current_direction = "none";
        }


        canvas.set_draw_color(Color::RGB(255, 255, 255));

        canvas.fill_rect(Rect::new(square_location[0], square_location[1], square_size_u32, square_size_u32));


        canvas.present();
        limiter = true;

        ::std::thread::sleep(Duration::new(1, 0));
    }

}



